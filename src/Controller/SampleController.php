<?php
   namespace App\Controller;
   use App\Controller\AppController;
   use Cake\ORM\TableRegistry;
   use Cake\Datasource\ConnectionManager;

   class SampleController extends AppController{
      public function index(){
        print_r('DB1<br>');
        $conn1 = ConnectionManager::get('default');
        $sql   = "SELECT * FROM  users";
        $query = $conn1->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();
        foreach($result as $res) {
            foreach($res as $r) {
                print_r($r);
                print_r('<br>');
            }
        }
        print_r('<br>');
        print_r('DB2<br>');
        $conn2 = ConnectionManager::get('db1');
        $sql = "SELECT * FROM books";
        $query = $conn2->prepare($sql);
        $query->execute();
        $result = $query->fetchall();
        foreach($result as $res) {
            foreach($res as $r) {
                print_r($r);
                print_r('<br>');
            }
        }
        print_r('<br>');
        print_r('DB3<br>');
        $conn3 = ConnectionManager::get('db2');
        $sql = "SELECT * FROM Students;";
        $query = $conn3->prepare($sql);
        $query->execute();
        $result = $query->fetchall();
        foreach($result as $res) {
            foreach($res as $r) {
                print_r($r);
                print_r('<br>');
            }
        }
      }
   }
?>